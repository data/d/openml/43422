# OpenML dataset: The-Bread-Basket

https://www.openml.org/d/43422

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The dataset belongs to "The Bread Basket" a bakery located in Edinburgh. The dataset has 20507 entries, over 9000 transactions, and 4 columns.
Content
The dataset has transactions of customers who ordered different items from this bakery online and the time period of the data is from 26-01-11 to 27-12-03.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43422) of an [OpenML dataset](https://www.openml.org/d/43422). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43422/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43422/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43422/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

